//
// Created by maks- on 28.10.2022.
//

#ifndef LAB1_TRANSFORM_H
#define LAB1_TRANSFORM_H
#include "image.h"

struct image rotate(const struct image source);

#endif //LAB1_TRANSFORM_H
