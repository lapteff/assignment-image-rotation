//
// Created by maks- on 26.10.2022.
//

#ifndef LAB1_BMP_H
#define LAB1_BMP_H

#include "../include/image.h"

#include  <stdint.h>
#include  <stdio.h>

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

struct bmp_header;
enum read_status read_bmp( FILE * in, struct image* img );
enum write_status write_bmp( FILE* out, struct image const * img );

#endif //LAB1_BMP_H
