//
// Created by maks- on 26.10.2022.
//

#ifndef LAB1_IMAGE_H
#define LAB1_IMAGE_H
#include <stdint.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};

struct image new_image_create(uint64_t width, uint64_t height);
void free_image(struct image *img);

#endif //LAB1_IMAGE_H
