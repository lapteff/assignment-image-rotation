//
// Created by maks- on 28.10.2022.
//

#ifndef LAB1_OPENCLOSE_H
#define LAB1_OPENCLOSE_H

#include <stdio.h>

enum open_file_status {
    FILE_OPEN_OK,
    FILE_WRITE_ERROR,
    FILE_READ_ERROR,
};

enum close_file_status{
    FILE_CLOSE_OK,
    FILE_CLOSE_ERROR,
};

enum open_file_status open_file(FILE ** file, const char * path, char* mode);
enum close_file_status close_file(FILE* file);


#endif //LAB1_OPENCLOSE_H
