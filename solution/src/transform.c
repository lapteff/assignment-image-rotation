//
// Created by maks- on 28.10.2022.
//

#include "../include/transform.h"
#include "../include/image.h"

static struct pixel get_source_pixel(const struct image source, uint64_t x, uint64_t y) {
    return source.data[y * source.width + x];
}

static uint64_t get_addr_by_coord(const struct image source, uint64_t x, uint64_t y){
    return x * source.height + (source.height - 1 - y);
}

struct image rotate(const struct image source){
    struct image new_img = new_image_create(source.height,source.width);
    for (uint64_t i = 0; i < source.height; i++) {
        for (uint64_t j = 0; j < source.width; j++) {
            new_img.data[get_addr_by_coord(source, j, i)] = get_source_pixel(source, j, i);
        }
    }
    return new_img;
}
