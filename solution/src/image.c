//
// Created by maks- on 26.10.2022.
//

#include "../include/image.h"
#include <malloc.h>

struct image new_image_create(uint64_t width, uint64_t height){
    return (struct image) {
        .width = width,
        .height = height,
        .data = malloc(height * width * sizeof(struct pixel))
    };
}

void free_image(struct image *img){
    free(img->data);
}
