#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/openclose.h"
#include "../include/transform.h"

#include <stdio.h>

int main( int argc, char** argv ) {

    if( argc != 3 ){
        fprintf(stderr, "Wrong amount of arguments");
        return 1;
    }

    char *path_in = argv[1];
    char *path_out = argv[2];
    struct image img = {0};

    FILE *file;
    if (open_file(&file, path_in, "rb") != FILE_OPEN_OK) {
        fprintf(stderr, "couldn't open the file");
        return 1;
    }
    if (read_bmp(file, &img) != READ_OK) {
        fprintf(stderr, "Couldn't convert bmp file");
        return 1;
    }

    struct image new_img = rotate(img);

    FILE *result_file;
    if (open_file(&result_file, path_out, "wb") != FILE_OPEN_OK) {
        fprintf(stderr, "couldn't open the file");
        return 1;
    }
    if (write_bmp(result_file, &new_img) != WRITE_OK) {
        fprintf(stderr, "Couldn't write the file");
        return 1;
    }

    if (close_file(file) != FILE_CLOSE_OK) {
        fprintf(stderr, "couldn't close the input file");
        return 1;
    }
    if (close_file(result_file) != FILE_CLOSE_OK) {
        fprintf(stderr, "couldn't close the output file");
        return 1;
    }

    free_image(&img);
    free_image(&new_img);

    return 0;
}
