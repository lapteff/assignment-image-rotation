//
// Created by maks- on 28.10.2022.
//

#include "../include/openclose.h"
#include <stdio.h>

enum open_file_status open_file(FILE ** file, const char * path, char* mode){
    *file = fopen(path,mode);
    if (file) {
        return FILE_OPEN_OK;
    }
    else {
        return FILE_WRITE_ERROR;
    }
}

enum close_file_status close_file(FILE* file){
    if (file) {
        fclose(file);
        return FILE_CLOSE_OK;
    }
    else {
        return FILE_CLOSE_ERROR;
    }
}
