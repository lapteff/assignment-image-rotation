//
// Created by maks- on 26.10.2022.
//

#include "../include/bmp.h"
#include "../include/image.h"
#include  <stdbool.h>
#include  <stdint.h>

static uint8_t calc_padding(uint32_t width){
    return width % 4;
}

static uint64_t size_of_image(const struct image* image){
    return (image->width * sizeof(struct pixel) + calc_padding(image->width)) * image->height;
}

struct __attribute__((packed)) bmp_header{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

static struct bmp_header create_header(struct image const *img) {
    return (struct bmp_header) {
        .bfType = 19778,
        .bfileSize = size_of_image(img) + sizeof(struct bmp_header),
        .bfReserved = 0,
        .bOffBits = 54,
        .biSize = 40,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biSizeImage = size_of_image(img),
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrImportant = 0,
        .biClrUsed = 0
    };
}

static bool check_header(const struct bmp_header *header){
    if (!(header->bfType == 19778 && header->biBitCount == 24)){
        return true;
    } return false;
}


enum read_status read_bmp(FILE* in, struct image* img){
    struct bmp_header header = {0};
    fread(&header,1,sizeof(struct bmp_header),in);
    if (check_header(&header)) {
        return READ_INVALID_HEADER;
    }

    struct image check_img = new_image_create(header.biWidth,header.biHeight);
    *img = check_img;

    const uint8_t padding = calc_padding(img->width);

    for (size_t i = 0; i <  header.biHeight ; i++){
        fread(&(img->data[i * img->width]), sizeof(struct pixel), img->width, in);
        if (ferror(in) != 0) {
            free_image(img);
            return READ_INVALID_BITS;
        }
        if (fseek(in, padding, SEEK_CUR) != 0) {
            free_image(img);
            return READ_INVALID_SIGNATURE;
        }
    }
    return READ_OK;

}

enum write_status write_bmp(FILE* out, struct image const* img){
    if (out == NULL){
        return WRITE_ERROR;
    }

    struct bmp_header header = create_header(img);
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)){
        return WRITE_ERROR;
    }

    const char zeros[] = {0, 0, 0};
    uint8_t padding = calc_padding(img->width);

    for (size_t i = 0; i < img->height; i++) {
        fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out);
        fwrite(zeros, 1, padding, out);
        if (ferror(out) != 0) return WRITE_ERROR;
    }
    return WRITE_OK;
}
